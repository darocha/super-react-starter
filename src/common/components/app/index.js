import React, { Component } from 'react';
//import { bindActionCreators } from 'redux';
//import * as CounterActions from '../../redux/actions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import TopNavbar from '../topnavbar';
import  { RoutesWithRouter } from '../../routes';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../fonts';
import './app.css';
import Drawer from '../drawer';

class App extends Component{
  
  render(){

    let {showDrawer}= this.props.main;
    
    return (
      <div className={ showDrawer ? 'show-drawer' : '' }>
        <Drawer />
        <div className="backdrop"/>
        <TopNavbar />
        <div className="main-container">
          <RoutesWithRouter />       
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state,
});

// make all actions available as props
// function mapDispatchToProps(dispatch) {
//   return bindActionCreators(CounterActions, dispatch);
// }

export default withRouter(connect(mapStateToProps)(App));
