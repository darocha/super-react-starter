import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { setDrawer } from '../../redux/actions/main';

class TopNavbar extends Component {

    openDrawer = () => {
      setDrawer(true);
    }
    
    render() {
        return (
          <React.Fragment>
          <div className="header-spacer" />
          <header>
            <nav className="main-topnavbar navbar navbar-expand-lg navbar-light bg-light">

          <button className="menu-toggle" onClick={this.openDrawer}>
            <span className="fa fa-bars" aria-hidden="true"></span>
            <FontAwesomeIcon  icon="bars" />
          </button>
          

            <a className="navbar-brand" href="/">Navbar</a>
          
            <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                <li className="nav-item">
                    <Link to="/" className="nav-link" >Home</Link>
                </li>
                <li className="nav-item">
                  <Link to="/blog" className="nav-link" >Blog</Link>
                </li>
                <li className="nav-item active">
                   <Link to="/contact" className="nav-link" >Contact</Link>
                </li>
                <li className="nav-item">
                  <Link to="/about" className="nav-link" >About</Link>
                </li>
                
                
              </ul>
              <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
              </form>
            </div>
          </nav>
          </header>
          </React.Fragment>
        );
    }

}

export default TopNavbar;