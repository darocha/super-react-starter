import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {setDrawer} from '../../redux/actions/main';
import Scrollbar from 'react-scrollbars-custom';

class Drawer extends Component {

    closeDrawer = () => {
      setDrawer(false);
    }
    
    showTracks = () => {
      this.scrollbar.trackXEl.style.opacity = 1;
      this.scrollbar.trackYEl.style.opacity = 1;

      this.dragEndTO && clearTimeout(this.dragEndTO);
      this.dragEndTO = null;
  };

  hideTracks = () => {
      if (
          this.scrollbar.thumbXEl.classList.contains("dragging") ||
          this.scrollbar.thumbYEl.classList.contains("dragging")
      ) {
          return;
      }

      this.scrollbar.trackXEl.style.opacity = 0;
      this.scrollbar.trackYEl.style.opacity = 0;

      this.dragEndTO && clearTimeout(this.dragEndTO);
      this.dragEndTO = null;
  };

  handleDragEnd = () => {
      this.dragEndTO = setTimeout(this.hideTracks, 500);
  };

    render(){
        return (
          <section className="main-drawer" >
              <button className="menu-toggle menu-close-button" onClick={this.closeDrawer}>
                <span className="fa fa-bars" aria-hidden="true"></span>
                <span className="sr-only">Close main menu</span>
                <FontAwesomeIcon  icon="times" />
              </button>
              <div className="drawer-header" />
                  <div className="drawer-container">
                <Scrollbar noScrollX={true} 
                    removeTrackYWhenNotUsed={false} 
                    removeTracksWhenNotUsed={false}
                    trackYProps={{style: {opacity: 0}, onMouseOver: this.showTracks, onMouseOut: this.hideTracks}}
                    thumbYProps={{onDragEnd: this.handleDragEnd}}
                    ref={ref => { this.scrollbar = ref; }}
                    onScrollStart={this.showTracks}
                    onScrollStop={this.hideTracks}
                    scrollDetectionThreshold={500}
                    style={ {width: '100%', height: '100%', minHeight: 300} } 
                  >
                    <nav className="main-menu">
                      <ul onClick={this.closeDrawer}>
                        <li><Link to="/" >Home</Link></li>
                        <li><Link to="/blog" >Blog</Link></li>
                        <li><Link to="/contact" >Contact</Link></li>
                        <li><Link to="/about" >About</Link></li>
                      </ul>
                    </nav>
                </Scrollbar>
                  </div>
              <div className="drawer-footer" />
          </section>
        );
    }

}

export default Drawer;