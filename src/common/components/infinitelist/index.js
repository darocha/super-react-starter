import React , {Component } from 'react';
import { Link } from 'react-router-dom';
import VirtualListFactory from 'react-virtual-list';
import InfiniteScroll from 'react-infinite-scroller';
//import smoothscroll from 'smoothscroll-polyfill';

//smoothscroll.polyfill();

const MyList = ({virtual}) => (
    <ul style={virtual.style} className="list-group list-group-flush">
      {virtual.items.map((item, i) => (
        <li key={`item_${i}`} className="list-group-item mainlist-listitem-height" >
          <div className="media">
              <Link to={item.url}>
                <div className="picture-container mr-3">
                        <img src={item.imageUrl} className="main-image vertical-align" alt="" />
                </div>
              </Link>  
              <div className="media-body">
                <h5 className="mt-0"><Link className="infinite-list-title" to={item.url}>{item.title}</Link></h5>
                <div>{item.text}</div>
              </div>
          </div>
        </li>
      ))}
    </ul>
);

let VirtualList = VirtualListFactory()(MyList);

class InfiniteList extends Component {

    constructor(props) {
        super(props);

        this.setContainerRef = element => {
          this.container = element;
        };
    };

    componentDidMount() {

        // let options = {
        //     container: this.container //, 
        //     // initialState: {
        //     //   firstItemIndex: 0, // show first ten items
        //     //   lastItemIndex: 9,  // during initial render
        //     // },
        // };
    
        VirtualList = VirtualListFactory()(MyList);
    
    }

    loadMore = () => {
      if (this.props.loadMore){
          this.props.loadMore();
      }
    }
      
    render() {
        return (
            <div ref={this.setContainerRef} id="infinite-scroller" className="infinite-scroller"  >
              <InfiniteScroll
              className={this.props.className}
                  pageStart={0}
                  loadMore={this.loadMore}
                  hasMore={true}
                  loader={<div className="flex flex-center loading-more-indicator" key={0}>Loading ...</div>}
                  useWindow={true}
              >
                  <VirtualList
                      items={this.props.data}
                      itemHeight={140}
                      itemBuffer={10}
                  />
              </InfiniteScroll>
            </div>
        )
    }

}

export default InfiniteList;
