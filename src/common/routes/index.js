import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router-dom';

import Home from '../pages/home';
import Blog from '../pages/blog';
import Article from '../pages/article';
import About from '../pages/about';
import Contact from '../pages/contact';
import NotFound from '../pages/notfound';

const Routes = () => {
  return (
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/about" component={About} />
        <Route path="/contact" component={Contact}  />
        <Route path="/blog" exact component={Blog} loadData />
        <Route path="/blog/:slug*" component={Article} loadData />
        <Route component={NotFound}  />
      </Switch>
  );
};

const mapStateToProps = (state) => ({
  ...state
}); 

const RoutesWithRouter = withRouter(connect(mapStateToProps)(Routes));

export default Routes;
export { RoutesWithRouter };
