import {store} from '../store';

export const SET_DATA = 'SET_DATA';
export const SET_DETAILS_DATA = 'SET_DETAILS_DATA';
export const SET_DRAWER = 'SET_DRAWER';

const set = (type, value) => ({
  type: type,
  payload: value,
});

export const setData = (data) => {
   store.dispatch(set(SET_DATA, data));
};

export const setDetailsData = (data) => {
    store.dispatch(set(SET_DETAILS_DATA, data));
};

export const setDrawer = (show) => {
  store.dispatch(set(SET_DRAWER, {show}));
};

