export const SET_DATA = 'SET_DATA';
export const SET_DETAILS_DATA = 'SET_DETAILS_DATA';
export const SET_DRAWER = 'SET_DRAWER';

let initialState = {
  page: 1,
  data: null,
  detailsData: null,
  showDrawer: false
}

const mainReducer = (state = initialState, action) => {

  switch(action.type){
      case SET_DATA: 
      return  { 
        ...state,
        data: action.payload
      };
      case SET_DETAILS_DATA:
      return { 
        ...state,
        detailsData: action.payload
      };
      case SET_DRAWER:
      return { 
        ...state,
        showDrawer: action.payload.show
      };
      default: return state;
  }

};

export default mainReducer;
