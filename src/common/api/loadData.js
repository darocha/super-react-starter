import 'isomorphic-fetch';

// const getRandomInt = (min, max) =>{
//   Math.floor(Math.random() * (max - min)) + min;
// }

const getData = () =>{
  return fetch(`https://jsonplaceholder.typicode.com/posts`)
  .then(res => {
    return res.json();
  })
  .then(data => {
    // only keep 10 first results
    return data.filter((_, idx) => idx < 10);
  });
} 

export const loadData = async (url) => {
 
  let data = await getData();

  for(let i =0, len=data.length;i<len; i++){
    data[i].url = `/${data[i].id}`;
  }
  return data;
  
};
