import { library } from '@fortawesome/fontawesome-svg-core';
import { 
    faTimes,
    faBars 
} from '@fortawesome/free-solid-svg-icons';

library.add(faBars);
library.add(faTimes);

