import React, {Component} from 'react';
import { connect } from 'react-redux';
// import {Link} from 'react-router-dom';
import './style.css';
import InfiniteList from '../../components/infinitelist';
import {setData} from '../../redux/actions/main';
import {loadData} from '../../api/loadData';

class Blog  extends Component {
  
//  getArticles = () =>{
//    let items = [];
//       for(var i=0; i<1000;i++){
//         items.push({url:`/blog/article${i}`, id: i, title : `Article ${i}`, imageUrl:'/', text:'aaa'});
//       }
//       return items;
//   }

  componentDidMount = async () => {
    if (window.__PRELOADED_STATE__ && window.__PRELOADED_STATE__.main && window.__PRELOADED_STATE__.data){
      // delete it already on redux
      delete window.__PRELOADED_STATE__;
    }
    else {
      let data = await loadData();
      setData(data);
    }
  }

  fetchMoreData = () => {

    console.log('fetchMoreData');

    if (this.props.items.length > 300) {    
        return;
    }

   // dataApi.loadData(this.props.items.concat(this.props.items));

};

refresh = () => {

}

handleClick = () =>{

}

onItemClick = (item)=>{

}

showProp = (prop) => {
  return true;
   // return this.props.show && this.props.show.indexOf(prop) !== -1;
}

loadMore = () => {

}

render(){

  let {data} = this.props.main;

  if(!data || data.length===0){
    return <div className="flex flex-column flex-full flex-center"><div><h1>No articles</h1></div></div>;
  }

  return (
      <InfiniteList loadMore={this.loadMore} data={data} className="list-view" />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state
}); 

export default connect(mapStateToProps)(Blog);
