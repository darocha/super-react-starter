import React from 'react';

export default ({ staticContext = {} }) => {
  staticContext.status = 404;
  return <div className="flex flex-column flex-full flex-center">
            <div>
                <h1>Oops, nothing here!</h1>
            </div>
         </div>;
};