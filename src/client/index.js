import React from 'react';
import { hydrate } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from '../common/redux/store';
import App from '../common/components/app';
import history from '../common/redux/history';
import { Router } from 'react-router';

const store = configureStore(window.__PRELOADED_STATE__);

hydrate(
  <Provider store={store}>
      <Router history={history} >
        <App />
      </Router>
  </Provider>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept('../common/components/app', () => {
    hydrate(
      <Provider store={store}>
        <Router history={history} >
          <App />
        </Router>
      </Provider>,
      document.getElementById('root')
    );
  });
}
