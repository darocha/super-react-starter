import App from '../common/components/app';
import { Provider } from 'react-redux';
import React from 'react';
import { StaticRouter } from 'react-router-dom';
import configureStore from '../common/redux/store';
import express from 'express';
import { loadData } from '../common/api/loadData';
import qs from 'qs';
import { renderToString } from 'react-dom/server';
import serialize from 'serialize-javascript';
import { matchPath, RouterContext } from 'react-router';
import Routes from '../common/routes';

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);

const server = express();

  server
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR));

  server.use(async (req, res) => {

    let routes = Routes();
    let matchRoute;
    let match ={};
    for (let i=0, len=routes.props.children.length; i<len; i++) 
    {
        match = matchPath(req.url, routes.props.children[i].props);
        if (match)
        {   
            matchRoute = routes.props.children[i].props;
            break;
        }
    }

    //console.log('match', match);
    //console.log('matchRoute', matchRoute);

    // Read params from the request, if exist
    const qParams = qs.parse(req.query);
    const page = parseInt(qParams.page, 10) || 1;

    let remoteData = [];

    if (match) {
      if (matchRoute.loadData)
       // load data from external api 
       remoteData = await loadData(match.url);
    }

    // console.log('remoteData', remoteData);

    // Compile an initial state
    const preloadedState = { 
      main: {
        data: remoteData,
        page: page
      }
    };

    // Create a new Redux store instance
    const store = configureStore(preloadedState);
  
    // Grab the initial state from our Redux store
    const finalState = store.getState();

    // console.log('store', store);
    
    // add fetched data to context so we can use it on the client
    const context = {};

    // Render the component to a string
    const markup = renderToString(
      <Provider store={store}>
        <StaticRouter location={req.url} context={context}>
          <App />
        </StaticRouter>
      </Provider>
    );

      res.send(`<!doctype html>
      <html lang="">
                      <head>
                          <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                          <meta charSet='utf-8' />
                          <title>Razzle Redux Example</title>
                          <meta name="viewport" content="width=device-width, initial-scale=1">
                          ${assets.client.css
                            ? `<link rel="stylesheet" href="${assets.client.css}">`
                            : ''}
                            ${process.env.NODE_ENV === 'production'
                              ? `<script src="${assets.client.js}" defer></script>`
                              : `<script src="${assets.client.js}" defer crossorigin></script>`}
                      </head>
                      <body>
                          <div id="root">${markup}</div>
                          <script>
                            window.__PRELOADED_STATE__ = ${serialize(finalState)}
                          </script>
                      </body>
                  </html>`);

  });


  // server.use((req, res) => {

  //   matchPath(
  //     { routes: Routes(), location: req.url },
  //     (error, redirectLocation, renderProps) => {

  //       if (error) {
  //         res.status(500).send(error.message);
  //       } else if (redirectLocation) {
  //         res.redirect(302, redirectLocation.pathname + redirectLocation.search);
  //       } else if (renderProps) {
          
  //         const context = {};
  //         const markup = renderToString(<RouterContext {...renderProps} />);
  
  //         if (context.url) {
  //           res.redirect(context.url);

  //         }
  //         else 
  //         {
  //           res.status(200).send(
  //             `<!doctype html>
  //                 <html lang="">
  //                 <head>
  //                     <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  //                     <meta charset="utf-8" />
  //                     <title>Welcome to Razzle</title>
  //                     <meta name="viewport" content="width=device-width, initial-scale=1">
  //                     ${
  //                       assets.client.css
  //                         ? `<link rel="stylesheet" href="${assets.client.css}">`
  //                         : ''
  //                     }
  //                     ${
  //                       process.env.NODE_ENV === 'production'
  //                         ? `<script src="${assets.client.js}" defer></script>`
  //                         : `<script src="${
  //                             assets.client.js
  //                           }" defer crossorigin></script>`
  //                     }
  //                 </head>
  //                 <body>
  //                 <div id="root">${markup}</div>
  //                 <script>
  //                   window.__PRELOADED_STATE__ = ${serialize(finalState)}
  //                 </script>
  //             </body>
  //             </html>`,
  //           );
  //         }
  //       } else {
  //         res.status(404).send('Not found');
  //       }
  //     },
  //   );
  // });
    



export default server;
